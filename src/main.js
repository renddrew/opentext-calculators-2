import Vue from 'vue'
import App from './App.vue'
import VueCurrencyInput from 'vue-currency-input';

Vue.config.productionTip = false

Vue.use(VueCurrencyInput, {
  globalOptions:{
    decimalLength: 0,
    currency: 'USD'
  }
});

new Vue({
  render: h => h(App),
}).$mount('#otarcalculator')
