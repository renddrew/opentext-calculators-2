﻿/*
*   global vars
*/
var MIN_NON_MOBILE_SIZE = 720;
var MIN_NON_TABLET_SIZE = 967;
var facetedsearchObject,upcomingEventsFacets,recordedEventsFacets;


(function($) {
    
    /*
    *   initialization
    */
    $(window).load(function() {
        
        $("body").opentextinit();
        
    });
    
    $.fn.opentextinit = function () {
        
        var $this = $(this);
        
        // initialize the Navigation's behavior.
        initNav();
        
        // initialize code to handle modals
        initModal();
        
        function searchTabs() {
            // Hide/Show Search Results based on Tab Logic function
            $(".event").hide();
            $(".event:first").show();
            
            $(".events-search-tabs ul li").click(function () {
                $(".event").hide();
                var activeTab = $(this).attr("data-rel");
                $("#" + activeTab).fadeIn();
                $("#" + activeTab).addClass("active");
                $(".events-search-tabs ul li").removeClass("active");
                $(this).addClass("active");
            });
        }
        
        if ($this.find('.events').length > 1) {
            window.has_searchTabs = true;
            searchTabs();
        } else {
            //do nothing
        }
        
        if ($this.find('.tabs3').length > 0) {
            tabLogic3();
        }
        if ($this.find('#tabs-accordian').length > 0) {
            tabLogic();
        }
        if ($this.find('.learn-more').length > 0) {
            tabLogic();
        }
        if ($this.find('.tabs2').length > 0) {
            tabLogic2();
        }
        
        //check if we are on a search results and pagination page
        if($this.find('.video-grid').length > 0 || $this.find('.customer-stories-search-results').length > 0 || $this.find('.js-faceted-results').length > 0){
            // send template and settings to facet maker
            facetedsearchObject = new $.FacetingItem();
            facetedsearchObject.facetelize(window.settings);
        }
        //check if upcoming events search is on page
        if($this.find('.upcoming .events-results-list').length > 0){
            // send template and settings to facet maker
            upcomingEventsFacets = new $.FacetingItem();
            upcomingEventsFacets.facetelize(window.settingsUpcoming);
        }
        //check if recorded events search is on page
        if($this.find('.recorded .events-results-list').length > 0){
            // send template and settings to facet maker
            recordedEventsFacets = new $.FacetingItem();
            recordedEventsFacets.facetelize(window.settingsRecorded);
        }
        // initialize search facet js if module is on page
        if ($this.find('.search-facets').length > 0) {
            initSearchFacets();
        }
        
        //check query params for tabs
        var tabsQueryValue = getParameterByName("tab");
        if(tabsQueryValue !== null){
            //subtrack one as we are on a base 0 array
            tabsQueryValue -= 1;
            //find tabs and make sure the number asked for isn't larger than available
            //if larger than available do nothing
            var tabsSelector = $('.tabs, .tabs2, .tabs3, .events-search-tabs');
            var tabsLiSelector = $('.tabs li, .tabs2 li, .tabs3 li, .events-search-tabs li');
            if(tabsSelector.length === 1){
                if(tabsLiSelector.length >= tabsQueryValue){
                    tabsLiSelector[tabsQueryValue].click();
                }
            }
            if(tabsSelector.length > 1){
                if($(tabsSelector[0]).find('li').length >= tabsQueryValue){
                    $(tabsSelector[0]).find('li')[tabsQueryValue].click();
                }
            }
        }
        
        $('.js-showhidetext-trigger a').on('click', function(e){
            e.preventDefault();
            $(this).closest('li').find('.js-showhidetext-target').toggle();
        });
        
        if ($('body').find('.scroll-to').length > 0) {
            $('.scroll-to').click(function () {
                var scrollToLink = $(this).attr('rel');
                $('html, body').animate({
                    scrollTop: $('#' + scrollToLink).offset().top
                }, 500);
                return false;
            });
        }
        
        $('.right-rail .module').last().addClass('last');
        
    };
    
    
    // get query parameter by name
    function getParameterByName(name) {
        var match = new RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
    
    
    /*
    *   modal js
    */
    function initModal() {
        
        $('.modal-overlay').click(function () {
            $('.final-modal-contents').hide();
            $('.modal-overlay').hide();
        });
        
        //modal popup from search results
        //for video and audio links
        $(document).on('click','.audioLightBox, .videoLightBox', function (e) {
            var jsLinkText = $(this).find(".hidden").text();
            e.preventDefault();
            var avWidth = 640;
            var avHeight = 360;
            var avRight = Math.round(($(window).width() - avWidth) * 0.5);
            //var width = ($(window).width()) * 0.5;
            //var height = ($(window).height()) * 0.5;
            
            $('.modal-overlay').show();
            
            // grab the modal code and move it next to the overlay.
            $('.final-modal-contents').html('<a class="close" href="#" style="display: block;">Close</a>');
            
            $('.modal-overlay').show();
            //$('.final-modal-contents').css('height', 'auto').css('width', 'auto').css('right', '28%').show();
            $('.final-modal-contents').css('height', avHeight + 'px').css('width', avWidth + 'px').css('right', avRight + 'px').show();
            // we anchor the close functionality only once the contents are already there.
            $('.close').click(function (event) {
                event.preventDefault();
                $('.final-modal-contents').hide();
                $('.modal-overlay').hide();
            });
            $.getScript( jsLinkText, function( data, textStatus, jqxhr ) {
                $('.final-modal-contents').append(objectStart + param1 + param2 + param3 + embed + objectEnd);
            });
        });
        
    }
    
    
    /*
    *   Search Facets js
    */
    function initSearchFacets() {
        
        // detect click and add or remove the active class with some fancy animations
        $('.vlist h3').click(function () {
            var activeElement = $(this).parent();
            var iconElement = $(this).children("span");
            
            // clicked on a +, hide the sub ul
            if (activeElement.hasClass('active')) {
                iconElement.toggleClass('icon-minus icon-plus');
                activeElement.find('.facets').slideUp(function () {
                    activeElement.removeClass('active');
                });
            }
            else {
                activeElement.find('.facets').hide();
                iconElement.toggleClass('icon-plus icon-minus');
                activeElement.addClass('active').find('.facets').slideDown();
            }
        });
        
        if($(window).width() <= MIN_NON_TABLET_SIZE){
            $('.vlist h3').click();
        }
        
    }
    
    
    /*
    *   Navigation js
    */
    function initNav() {
        
        // make dropdown slide in or out for left nav
        $('.left-nav a.dropdown').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('.left-nav .hidden-list').slideUp(500, function () {
                    $('.left-nav .hidden-list').addClass('active');
                });
            }
            else {
                $(this).addClass('active');
                $('.left-nav .hidden-list').slideDown(500, function () {
                    $('.left-nav .hidden-list').removeClass('active');
                });
            }
        });
        
        //set nav to proper color
        var activeItem = $(".sub-nav .active");
        activeItem.parent().parent().css("background-color", activeItem.css("background-color"));
        if($('.left-nav').length > 0){
            //make sure left nav continues to show up on desktop
            $(window).resize(function () {
                if(this.leftNavDisplay) {
                    clearTimeout(this.leftNavDisplay);
                }
                this.leftNavDisplay = setTimeout(function() {
                    if($(window).width() > MIN_NON_TABLET_SIZE){
                        $('.left-nav .hidden-list').show();
                    }
                }, 500);
            });
        }
        
    }
    
})(jQuery);


/*
*   is the hide unhide logic for the tabs and when they turn into accordians in mobile
*   lightly refactored oct 2013 to support multiple tabsets on a single page without changing existing markup.
*   every tab will still have to have a unique ID because that's how the DOM works...
*   ...but this should make for a little less hoops-jumping.
*/
function tabLogic() {
    
    // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $('.tab-container').each( function() {
        var $t = $(this); // the wrapper of all the tabbed content
        var $tabset = $t.find('.tab-content'); // the set of chunks of tabbed content
        var $tablinkset = $t.siblings('ul.tabs').find('li'); // the set of buttons to hide/reveal
        var $tabdrawerset = $t.find('.tab-drawer-heading'); // the set of buttons to fold/unfold
        $tabset.hide();
        if (!$t.find('.tab-drawer-heading').is(':visible')) {
            $tabset.eq(0).show();
        }
        // when in tab (desktop) mode
        $tablinkset.click( function() {
            $tabset.hide().removeClass('active');
            $tablinkset.removeClass('active');
            $(this).addClass('active');
            var activeTab = $(this).attr('data-rel');
            $("#" + activeTab).fadeIn().addClass('active');
            $t.find(".tab-drawer-heading").removeClass("d-active");
            $t.find(".tab-drawer-heading[data-rel^='" + activeTab + "']").addClass("d-active");
        });
        // when in drawer (mobile) mode
        $tabdrawerset.click( function() {
            $tabset.hide().removeClass('active');
            if ($(this).hasClass('d-active')) {
                $tabdrawerset.removeClass("d-active").find('span.icon-minus').removeClass('icon-minus').addClass('icon-plus');
                $("ul.tabs li").removeClass("active");
                $tablinkset.removeClass('active');
            } else {
                var d_activeTab = $(this).attr("data-rel");
                $("#" + d_activeTab).fadeIn();
                $tabdrawerset.removeClass("d-active").find('span.icon-minus').removeClass('icon-minus').addClass('icon-plus');
                $(this).addClass("d-active").find('span.icon-plus').removeClass('icon-plus').addClass('icon-minus');
                $tablinkset.removeClass("active");
                $tablinkset.filter("[data-rel^='" + d_activeTab + "']").addClass("active");
                // animate scrolling to the section we just opened
                $('html, body').animate({
                    scrollTop: $("#" + d_activeTab).offset().top - $tabdrawerset.outerHeight()
                }, 750);
            }
        });
        // Extra class "tab-last" to add border to right side of last tab
        $tablinkset.last().addClass("tab-last");
    });
    
}


/*
*   tabs2 logic for pages like product details page that uses 2col or 3col layout
*   eventually this will contain more complex functionality for more tabs that can be displayed in it's width
*/
function tabLogic2() {
    
    $(".tabs2-content").hide();
    $(".tabs2-content:first").show();
    
    /* if in tab mode */
    $("ul.tabs2 li:not('.controls')").click(function () {
        // console.log("clickity");
        $(".tabs2-content").hide();
        var activeTab = $(this).attr("data-rel");
        //console.log(activeTab);
        $("#" + activeTab).fadeIn();
        $("ul.tabs2 li").removeClass("active");
        $(this).addClass("active");
    });
    
}


/*
*   tabs3 is for customer carousel (no longer carousel) section
*/
function tabLogic3() {
    
    $(".carousel-content").hide();
    $(".carousel-content:first").show();
    $("ul.tabs3 li:first").addClass("active");
    
    /* if in tab mode */
    $("ul.tabs3 li").click(function () {
        // console.log("clickity");
        $(".carousel-content").hide();
        var activeTab = $(this).attr("data-rel");
        //console.log(activeTab);
        $("#" + activeTab).fadeIn();
        $("ul.tabs3 li").removeClass("active");
        $(this).addClass("active");
    });
    
}


/* -------------------------------------------------------------------------- */


jQuery(function($) {
    
    // Primary object to contain functionality
    var ot = {};
    
    
    // Add spans around each word in a string of text
    $('.js-add-spans').each(function() {
        
        var brArray = $.trim($(this).html()).split('<br>'),
            brArrayLength = brArray.length - 1,
            spanText = '',
            textArray;
            
        $(brArray).each(function(index, item) {
            
            item = item.replace(/\s\s+/g, ' ');
            textArray = $.trim(item).split(' ');
            
            $(textArray).each(function(index, item) {
                spanText += '<span>' + item + '</span>';
            });
            
            if (index < brArrayLength) {
                spanText += '<br>';
            }
            
        });
        
        $(this).html(spanText);
        
    });
    
    
    // Show menu on campaign page
    ot.CampaignMenu = {
        
        $navButton     : null,
        $navMenu       : null,
        $target        : $( ".new-campaign" ),
        region         : $( "html" )[0].lang,
        buttonClass    : "ot-site-button",
        menuClass      : "ot-site-menu",
        menuId         : "site-menu",
        activeClass    : "active",
        container      : ".top-nav .content-wrap",
        devItem        : [
                "Developers",
                "https://developer.opentext.com"
            ],
        navItems       : {
            "en" : [[
                    "Products &amp; Solutions",
                    "https://www.opentext.com/products-and-solutions"
                ],[
                    "Services",
                    "https://www.opentext.com/services"
                ],[
                    "Support",
                    "https://www.opentext.com/support"
                ],[
                    "About",
                    "https://www.opentext.com/about"
                ],[
                    "Contact us",
                    "https://www.opentext.com/about/contact-us/contact-opentext"
                ]],
            "en-au" : [[
                    "What we do",
                    "https://www.opentext.com.au/what-we-do"
                ],[
                    "Who we are",
                    "https://www.opentext.com.au/who-we-are"
                ],[
                    "Videos",
                    "https://www.opentext.com.au/videos"
                ],[
                    "Customer stories",
                    "https://www.opentext.com.au/customer-stories"
                ],[
                    "Support",
                    "https://www.opentext.com.au/support"
                ],[
                    "Events and webinars",
                    "https://www.opentext.com.au/events-and-webinars"
                ],[
                    "Contact us",
                    "https://www.opentext.com.au/who-we-are/contact-us/contact-opentext"
                ]],
            "en-gb" : [[
                    "What we do",
                    "https://www.opentext.co.uk/what-we-do"
                ],[
                    "Who we are",
                    "https://www.opentext.co.uk/who-we-are"
                ],[
                    "Videos",
                    "https://www.opentext.co.uk/videos"
                ],[
                    "Customer stories",
                    "https://www.opentext.co.uk/customer-stories"
                ],[
                    "Support",
                    "https://www.opentext.co.uk/support"
                ],[
                    "Events and webinars",
                    "https://www.opentext.co.uk/events-and-webinars"
                ],[
                    "Contact us",
                    "https://www.opentext.co.uk/who-we-are/contact-us/contact-opentext"
                ]],
            "de-de" : [[
                    "Was wir tun",
                    "https://www.opentext.de/was-wir-tun"
                ],[
                    "Wer wir sind",
                    "https://www.opentext.de/wer-wir-sind"
                ],[
                    "Videos",
                    "https://www.opentext.de/videos"
                ],[
                    "Kunden",
                    "https://www.opentext.de/kunden"
                ],[
                    "Support",
                    "https://www.opentext.com/support"
                ],[
                    "Veranstaltungen und Webinare",
                    "https://www.opentext.de/veranstaltungen-und-webinare"
                ],[
                    "Kontakt",
                    "https://www.opentext.de/wer-wir-sind/contact-us/contact-opentext"
                ]],
            "fr-fr" : [[
                    "Notre savoir-faire",
                    "https://www.opentext.fr/notre-savoir-faire"
                ],[
                    "Notre société",
                    "https://www.opentext.fr/notre-societe"
                ],[
                    "Vidéos",
                    "https://www.opentext.fr/videos"
                ],[
                    "Références",
                    "https://www.opentext.fr/references"
                ],[
                    "Support",
                    "https://www.opentext.fr/support"
                ],[
                    "Événements et webinaires",
                    "https://www.opentext.fr/evenements-et-webinaires"
                ],[
                    "Contactez-nous",
                    "https://www.opentext.fr/notre-societe/contact-us/contact-opentext"
                ]],
            "ja-jp" : [[
                    "ソリューション紹介",
                    "https://www.opentext.jp/what-we-do"
                ],[
                    "会社概要",
                    "https://www.opentext.jp/who-we-are"
                ],[
                    "ビデオ",
                    "https://www.opentext.jp/videos"
                ],[
                    "顧客事例",
                    "https://www.opentext.jp/customer-stories"
                ],[
                    "サポート",
                    "https://www.opentext.jp/support"
                ],[
                    "イベント＆セミナー",
                    "https://www.opentext.jp/events-and-webinars"
                ],[
                    "お問い合わせ",
                    "https://www.opentext.jp/who-we-are/contact-us/contact-opentext"
                ]],
            "pt-br" : [[
                    "O que fazemos",
                    "https://www.opentext.com.br/o-que-fazemos"
                ],[
                    "Quem somos",
                    "https://www.opentext.com.br/quem-somos"
                ],[
                    "Videos",
                    "https://www.opentext.com.br/videos"
                ],[
                    "Histórias de clientes",
                    "https://www.opentext.com.br/historias-de-clientes"
                ],[
                    "Suporte",
                    "https://www.opentext.com.br/suporte"
                ],[
                    "Eventos &amp; webinários",
                    "https://www.opentext.com.br/eventos-and-webinarios"
                ],[
                    "Fale conosco",
                    "https://www.opentext.com.br/quem-somos/contact-us/contact-opentext"
                ]],
            "sv-se" : [[
                    "Vad vi gör",
                    "https://www.opentext.se/vad-vi-goer"
                ],[
                    "Vilka vi är",
                    "https://www.opentext.se/vilka-vi-aer"
                ],[
                    "Videos",
                    "https://www.opentext.se/videos"
                ],[
                    "Kundberättelser",
                    "https://www.opentext.se/kundberaettelser"
                ],[
                    "Support",
                    "https://www.opentext.se/support"
                ],[
                    "Evenemang och webbseminarier",
                    "https://www.opentext.se/evenemang-och-webbseminarier"
                ],[
                    "Kontakta oss",
                    "https://www.opentext.se/vilka-vi-aer/contact-us/kontaktformulaer"
                ]]
            },
            
        init : function() {
            if ( this.$target.length && this.navItems[ this.region ] ) {
                this
                    .createButton()
                    .createMenu()
                    .bindUIActions();
            }
        },
        
        createButton : function() {
            this.$navButton = $( "<a>", {
                    "class" : this.buttonClass,
                    href    : "#" + this.menuId
                }).append(
                    "<span></span>" +
                    "<span></span>" +
                    "<span></span>"
                ).appendTo( this.container );
                
            return this;
        },
        
        createMenu : function() {
            this.$navMenu = $( "<ul>", {
                    "class" : this.menuClass,
                    id      : this.menuId
                }).append(
                    this.createMenuItems()
                ).appendTo( this.container );
                
            return this;
        },
        
        createMenuItems : function() {
            var i,
                len,
                array = [],
                items = this.navItems[ this.region ];
                
            //items.push( this.devItem );
            items.splice( items.length - 1, 0, this.devItem );
            
            for ( i = 0, len = items.length; i < len; i++ ) {
                array.push(
                    "<li><a href=\"" + items[i][1] + "\">" +
                    items[i][0] +
                    "</a></li>"
                );
            }
            
            return array.join( "" );
        },
        
        bindUIActions : function() {
            var o = this;
            
            o.$navButton.on( "click", function( e ) {
                e.preventDefault();
                e.stopPropagation();
                $( this ).add( o.$navMenu ).toggleClass( o.activeClass );
            });
            
            $( document ).on( "click", function() {
                if ( o.$navButton.hasClass( o.activeClass ) ) {
                    o.$navButton.click();
                }
            });
        }
        
    };
    
    
    // Set/save facets from/to URL
    (function() {
        
        var api = ot.Facet = {},
            s; // api settings
            
        api.settings = {
            paramName  : 'state',
            urlParams  : {},
            fsObject   : 'facetedsearchObject',
            fsSettings : window.settings
        };
        
        api.init = function() {
            // Check if fs object/settings defined before proceeding
            if (typeof api.settings.fsSettings === 'undefined') {
                return;
            }
            
            s = api.settings;
            api.getUrlParams();
            api.setState();
            api.bindUIActions();
        };
        
        // Get params from URL
        api.getUrlParams = function() {
            if (window.location.search) {
                var i, len, tmp,
                    params = window.location.search.slice(1).split('&');
                    
                for (i = 0, len = params.length; i < len; i++) {
                  tmp = params[i].split('=');
                  s.urlParams[decodeURIComponent(tmp[0])] = (tmp.length > 1) ? decodeURIComponent(tmp[1]) : '';
                }
            }
        };
        
        // Update settings and set history state
        api.setState = function() {
            // Update settings if param found in URL
            if (s.urlParams[s.paramName]) {
                s.fsSettings.state = JSON.parse(s.urlParams[s.paramName]);
            }
            
            // Set initial settings in history
            if (typeof history.replaceState !== 'undefined') {
                history.replaceState(s.fsSettings.state, null, null);
            }
            
            // Select orderBy option if set
            $(s.fsSettings.facetSelector).bind('facetuicreated', function() {
                if (typeof s.fsSettings.state.orderBy !== 'undefined') {
                    $('#orderby_' + s.fsSettings.state.orderBy).prop('selected', true);
                }
            });
        };
        
        // Update settings based on UI actions
        api.bindUIActions = function() {
            // Update when facet clicked
            $(s.fsSettings.facetSelector).bind('facetedsearchfacetclick', function() {
                api.updateUrl();
            });
            
            // Update when orderby chosen
            $(s.fsSettings.facetSelector).bind('facetedsearchorderby', function() {
                api.updateUrl();
            });
            
            // Update when all filters are removed
            $('.deselectstartover').on('click', function() {
                api.updateUrl();
            });
            
            // Update when used filters are removed
            $('.usedFilters').on('click', function(event) {
                if ($(event.target).hasClass('uselistitem')) {
                    api.updateUrl();
                }
            });
            
            // Update when browser's back/forward buttons clicked
            window.onpopstate = function(event) {
                // Check if not initial history state before proceeding
                if (event.state === null) {
                    return;
                }
                
                // Update settings with history state
                s.fsSettings.state = event.state;
                
                // Update faceted search UI with new settings
                window[s.fsObject].facetelize(s.fsSettings);
                
                // Set orderBy option to selected
                $('#orderby_' + s.fsSettings.state.orderBy).prop('selected', true);
            };
        };
        
        // Update URL without refreshing the page
        api.updateUrl = function() {
            var state  = s.fsSettings.state,
                title  = window.document.title,
                hash   = window.location.hash,
                newUrl = window.location.pathname;
                
            // Convert settings to string and assign object
            s.urlParams[s.paramName] = JSON.stringify(s.fsSettings.state);
            
            // Construct new location string
            newUrl += '?';
            newUrl += Object.keys(s.urlParams).map(function(key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(s.urlParams[key]);
            }).join('&');
            newUrl += hash;
            
            // Update URL
            if (typeof history.pushState !== 'undefined') {
                history.pushState(state, title, newUrl);
            }
        };
        
    }());
    
    
    // Set/save facets from/to URL
    (function() {
        var api = ot.EventFacet = {},
            upevntsetting, webinarsetting, // api settings
            currentTab = 'upcoming';
            
        api.upcomingeventSettings = {
            paramName  : 'ue_state',
            urlParams  : {},
            fsObject   : 'upcomingEventsFacets',
            fsSettings : window.settingsUpcoming
        };
        
        api.recorderwebinarSettings = {
            paramName  : 'rw_state',
            urlParams  : {},
            fsObject   : 'recordedEventsFacets',
            fsSettings : window.settingsRecorded
        };
        
        api.initEvents = function() {
            // Check if fs object/settings defined before proceeding
            if (typeof api.upcomingeventSettings.fsSettings === 'undefined' || typeof api.recorderwebinarSettings.fsSettings === 'undefined' ) {
                return;
            }
            upevntsetting = api.upcomingeventSettings;
            webinarsetting = api.recorderwebinarSettings;
            api.getUrlParams();
            
            api.setState(upevntsetting);    
            api.bindUIActions(upevntsetting);
            
            api.setState(webinarsetting);   
            api.bindUIActions(webinarsetting);
        };
        
        // Get params from URL: It needs to be changed to set the params based on the tab being loaded
        api.getUrlParams = function() {
            if (window.location.search) {
                var i, len, tmp,
                    params = window.location.search.slice(1).split('&');
                    var tmpUrlParams ={};
                for (i = 0, len = params.length; i < len; i++) {
                  tmp = params[i].split('=');
                  tmpUrlParams[decodeURIComponent(tmp[0])] = (tmp.length > 1) ? decodeURIComponent(tmp[1]) : '';
                }
                //set corresponding settings and the urlParams Array
                if(!$.isEmptyObject(tmpUrlParams)){                 
                    
                    upevntsetting.urlParams = tmpUrlParams;
                    api.setState(upevntsetting);    
                    
                    webinarsetting.urlParams = tmpUrlParams;                    
                    api.setState(webinarsetting);   
                    api.updateUrl();
                } 
            }
        };
        
        // Update settings and set history state
        api.setState = function(eventsettings) {
            // Update settings if param found in URL
            
            if (eventsettings.urlParams[eventsettings.paramName]) {
                eventsettings.fsSettings.state = JSON.parse(eventsettings.urlParams[eventsettings.paramName]);
            }
            
            // Set initial settings in history
            if (typeof history.replaceState !== 'undefined') {
                history.replaceState(eventsettings.fsSettings.state, null, null);
            }
            
            // Select orderBy option if set
            $(eventsettings.fsSettings.facetSelector).bind('facetuicreated', function() {
                if (typeof eventsettings.fsSettings.state.orderBy !== 'undefined') {
                    if($('#search-result1').find('.orderbyitem')){                      
                        $('#search-result1').find('.orderbyitem').each(function(){
                            var id = this.id.substr(8);
                            if (api.upcomingeventSettings.fsSettings.state.orderBy === id) {
                                $(this).addClass("activeorderby");
                                console.log("id is" + id +$('#' + id));
                                $(this).prop('selected', true);     
                            } else {
                                $(this).removeClass("activeorderby");
                            }
                        });
                    }
                    if($('#search-result2').find('.orderbyitem')){                      
                        $('#search-result2').find('.orderbyitem').each(function(){
                            var id = this.id.substr(8);
                            if (api.recorderwebinarSettings.fsSettings.state.orderBy === id) {
                                $(this).addClass("activeorderby");
                                console.log("id is" + id +$('#' + id));
                                $(this).prop('selected', true);     
                            } else {
                                $(this).removeClass("activeorderby");
                            }
                        });
                    }
                }
            });
        };
        
        // Update settings based on UI actions
        api.bindUIActions = function(eventsettings) {
            // Update when facet clicked
            $(eventsettings.fsSettings.facetSelector).bind('facetedsearchfacetclick', function() {
                api.updateUrl();
            });
            
            // Update when orderby chosen
            $(eventsettings.fsSettings.facetSelector).bind('facetedsearchorderby', function() {
                api.updateUrl();
            });
            
            // Update when all filters are removed
            $('.deselectstartover').on('click', function() {
                api.updateUrl();
            });
            
            // Update when used filters are removed
            $('.usedFilters').on('click', function(event) {
                if ($(event.target).hasClass('uselistitem')) {
                    api.updateUrl();
                }
            });
            
            // Update when browser's back/forward buttons clicked
            window.onpopstate = function(event) {
                // Check if not initial history state before proceeding
                if (event.state === null) {
                    return;
                }
                var es;
                if(currentTab === 'upcoming'){
                    es = upevntsetting;
                } else {
                    es = webinarsetting;
                }
                // Update settings with history state
                es.fsSettings.state = event.state;
                
                // Update faceted search UI with new settings
                window[es.fsObject].facetelize(es.fsSettings);
                
                // Set orderBy option to selected
                $('#orderby_' + es.fsSettings.state.orderBy).prop('selected', true);
            };
        };
        
        // Update URL without refreshing the page
        api.updateUrl = function() {
            
            var u_state  = upevntsetting.fsSettings.state,
                r_state = webinarsetting.fsSettings.state,
                title  = window.document.title,
                hash   = window.location.hash,
                newUrl = window.location.pathname;
                
            if(u_state.filters && u_state.filters.undefined){
                delete u_state.filters.undefined;
            }
            
            if(r_state.filters && r_state.filters.undefined){
                delete r_state.filters.undefined;
            }
            // Construct new location string
            newUrl += '?';  
            // Convert settings to string and assign object
            upevntsetting.urlParams[upevntsetting.paramName] = JSON.stringify(u_state);
            
            newUrl += Object.keys(upevntsetting.urlParams).map(function(key) {
                if(key === upevntsetting.paramName){
                    return encodeURIComponent(key) + '=' + encodeURIComponent(upevntsetting.urlParams[key]);
                }
            }).join('&');
            newUrl += '&';
            webinarsetting.urlParams[webinarsetting.paramName] = JSON.stringify(r_state);
            
            newUrl += Object.keys(webinarsetting.urlParams).map(function(key) {
                if(key === webinarsetting.paramName){
                    return encodeURIComponent(key) + '=' + encodeURIComponent(webinarsetting.urlParams[key]);
                }
            }).join('&');
            
            newUrl += hash;
            
            // Update URL
            if (typeof history.pushState !== 'undefined') {
                if(currentTab === 'upcoming'){
                    history.pushState(u_state, title, newUrl);
                } else {
                    history.pushState(r_state, title, newUrl);
                }
            }
        };
    }());
    
    
    // Initializations
    ot.CampaignMenu.init();
    ot.Facet.init();
    ot.EventFacet.initEvents();
    
});

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}
