( function( $, otApi ) {
    
    // Exit if jQuery not loaded
    if ( !$ ) {
        return;
    }
    
    
    /* Globals
    ------------------------------------------------------------------------- */
    
    otApi.ACTIVE_BREAKPOINT = "";
    otApi.ROOT_PATH         = otApi.ROOT_PATH || "";
    otApi.RUN_ON_RESIZE     = {};
    otApi.URL_PARAMS        = {};
    
    
    /* Utilities
    ------------------------------------------------------------------------- */
    
    // Add CSS files to head element
    otApi.addCSSFiles = function( cssFiles ) {
        var i;
        
        for ( i in cssFiles ) {
            $( "<link>", {
                rel  : "stylesheet",
                href : cssFiles[i]
                }).appendTo( "head" );
        }
        
        return this;
    };
    
    // Add JS files via Ajax and run a callback function
    otApi.addJSFiles = function( jsFiles, callback, args ) {
        var i,
            count = 0;
            
        function runCallback() {
            if ( callback && ++count === jsFiles.length ) {
                callback( args );
            }
        }
        
        for ( i in jsFiles ) {
            $.ajax({
                 method   : "GET",
                 dataType : "script",
                 url      : jsFiles[ i ],
                 success  : runCallback,
                 cache    : true
             });
        }
        
        return this;
    };
    
    // Get params from URL
    otApi.getUrlParams = function() {
        var i,
            array,
            params,
            urlParams = {};
            
        if ( window.location.search ) {
            params = window.location.search.slice( 1 ).split( "&" );
            
            for ( i in params ) {
              array = params[ i ].split( "=" );
              
              urlParams[ decodeURIComponent( array[ 0 ] ) ] =
                ( array.length > 1 ) ? decodeURIComponent( array[ 1 ] ) : "";
            }
        }
        
        return urlParams;
    };
    
    // Check for mobile view
    otApi.isMobile = function() {
        if ( otApi.ACTIVE_BREAKPOINT === "small" ) {
            return true;
        } else {
            return false;
        }
    };
    
    
    /* Breakpoint Watcher (get breakpoint value from CSS)
    ------------------------------------------------------------------------- */
    
    otApi.BreakpointWatcher = {
        
        // Settings
        $element     : null,
        elementClass : "breakpoint-watcher",
        
        // Add targeting element
        addElement : function() {
            return $( "<meta>", {
                "class" : this.elementClass
            }).appendTo( "head" );
        },
        
        // Get breakpoint value
        getBreakpoint : function() {
            this.$element = this.$element || this.addElement();
            return this.$element.css( "font-family" ).replace( /['"]/g, "" );
        }
        
    };
    
    
    /* Carousel (using Owl Carousel plugin)
    ------------------------------------------------------------------------- */
    
    otApi.Carousel = {
        
        // Settings
        $trigger    : $( ".js-carousel" ),
        dataOptions : "options",
        uiClasses   : ".owl-nav, .owl-dots",
        hiddenClass : "js-hidden",
        owlClass    : "owl-carousel owl-theme",
        owlStop     : "stop.owl.autoplay",
        mainElement : "main",
        cssFiles    : [
            otApi.ROOT_PATH + "/file_source/OpenText/Scripts/owl.carousel2-ot.min.css"
        ],
        jsFiles     : [
            otApi.ROOT_PATH + "/file_source/OpenText/Scripts/owl.carousel2.min.js"
        ],
        
        // Initialize
        init : function() {
            if ( this.$trigger.length ) {
                otApi
                    .addCSSFiles( this.cssFiles )
                    .addJSFiles( this.jsFiles, this.applyCarousel );
            }
        },
        
        // Unhide carousel items
        unhideCarousel : function( $carousel ) {
            $carousel
                .find( "." + this.hiddenClass )
                .add( $carousel )
                .removeClass( this.hiddenClass );
                
            return;
        },
        
        // Apply carousel
        applyCarousel : function() {
            var o = otApi.Carousel;
            
            o.$trigger.each( function() {
                var $carousel = $( this ),
                    options   = $carousel.data( o.dataOptions ) || {};
                    
                // Ignore for mobile
                if ( options.mobileFriendly === false && otApi.isMobile() ) {
                    o.unhideCarousel( $carousel );
                    return;
                }
                
                // Callback initializations
                options.onInitialize = function() {
                    // Unhide items
                    o.unhideCarousel( $carousel );
                    
                    // Stop rotation and pause player if UI clicked
                    $( o.mainElement )
                        .on( "click", o.uiClasses, function() {
                            $( this )
                                .closest( o.$trigger )
                                .trigger( o.owlStop )
                                .find( "audio, video" )
                                .each( function() {
                                    this.pause();
                                });
                        });
                };
                
                // Apply carousel
                try {
                    $carousel
                        .addClass( o.owlClass )
                        .owlCarousel( options );
                }
                catch( error ) {
                    $carousel
                        .removeClass( o.hiddenClass );
                }
            });
        }
        
    };
    
    
    /* Clickable (attach click event to a parent element)
    ------------------------------------------------------------------------- */
    
    otApi.Clickable = {
        
        // Settings
        $trigger   : $(".js-clickable"),
        
        // Initialize
        init : function() {
            
            if (this.$trigger.length) {
                this.bindUIActions();
            }
            
        },
        
        // Handle click events
        bindUIActions : function() {
            
            // Add click event to parent element
            $(this.$trigger)
                .on("click", function(event) {
                    
                    // Ignore if text selected
                    if (window.getSelection().toString()) {
                        return false;
                    }
                    
                    // Use first child anchor as link
                    var link = $(this).find("a").get(0);
                    
                    if (typeof link !== "undefined") {
                        
                        // Check if ctrl or cmd key held when clicked
                        // or target is '_blank'
                        if (event.ctrlKey ||
                            event.metaKey ||
                            link.target === "_blank") {
                            // Open link in new tab
                            window.open(link.href, "_blank");
                        } else {
                            // Open link in same tab
                            window.location = link.href;
                        }
                        
                    } else {
                        return false;
                    }
                    
                })
                // Ignore click event on parent element if child anchor clicked
                .find("a").on("click", function(event) {
                    event.stopPropagation();
                });
                
        }
        
    };
    
    
    /* Grid overlay (toggle grid overlay)
    ------------------------------------------------------------------------- */
    
    otApi.GridOverlay = {
        
        // Settings
        $overlay     : null,
        overlayClass : "grid-overlay",
        activeClass  : "active",
        
        // Initialize
        init : function() {
            
            // Append overlay HTML
            this.$overlay = $( "<div>", {
                "class" : this.overlayClass
            }).appendTo( "body" );
            
            // Bind UI actions
            this.bindUIActions();
            
            // Resize overlay height on window resize
            otApi.RUN_ON_RESIZE.GridOverlayResize = function() {
                otApi.GridOverlay.resizeOverlayHeight();
            };
            
        },
        
        // Bind UI actions
        bindUIActions : function() {
            
            // Toggle overlay on keydown using Command + Option + g
            $( document ).keydown( function( event ) {
                if ( event.altKey && event.metaKey && event.keyCode === 71 ) {
                    otApi.GridOverlay.toggleOverlay();
                }
            });
            
        },
        
        // Show/hide overlay
        toggleOverlay : function() {
            
            this.$overlay.toggleClass( this.activeClass );
            this.setOverlayHeight();
            
        },
        
        // Set overlay height
        setOverlayHeight : function() {
            
            this.$overlay.height( $( document ).height() );
            
        },
        
        // Resize overlay height
        resizeOverlayHeight : function() {
            
            // Resize only if overlay is shown
            if ( this.$overlay.hasClass( this.activeClass ) ) {
                // Reset overlay height to avoid altering document height
                this.$overlay.height( "auto" );
                // Set overlay height
                this.setOverlayHeight();
            }
            
        }
        
    };
    
    
    /* Hamburger button
    ------------------------------------------------------------------------- */
    
    otApi.Hamburger = {
        
        // Settings
        $trigger    : $( ".js-toggle-site-menu" ),
        activeClass : "active",
        
        // Initialize
        init : function() {
            
            if ( this.$trigger.length ) {
                this.bindUIActions();
            }
            
        },
        
        // Bind UI actions
        bindUIActions : function() {
            
            // Show/hide site nav on click
            this.$trigger.on( "click", function() {
                
                var $this = $( this ),
                    $menu = $( "#" + $this.attr( "aria-controls" ) );
                    
                // Toggle hamburger button and site menu
                $this
                    .add( $menu )
                    .toggleClass( otApi.Hamburger.activeClass );
                    
                // Toggle aria expanded
                $this.attr( "aria-expanded", function( index, attr ) {
                    if ( attr === "true" ) {
                        return "false";
                    } else {
                        return "true";
                    }
                });
                
            });
            
        }
        
    };
    
    
    /* Iframes (delay loading iframes for page speed performance)
    ------------------------------------------------------------------------- */
    
    otApi.Iframes = {
        
        // Settings
        $trigger : $( ".js-load-iframe" ),
        dataSrc  : "data-src",
        
        // Initialize
        init : function() {
            
            if ( this.$trigger.length ) {
                
                // Set iframe src on window load
                $( window ).on( "load", function() {
                    
                    otApi.Iframes.$trigger.each( function( index ) {
                        otApi.Iframes.setSrc( $( this ), index );
                    });
                    
                });
                
            }
            
        },
        
        // Set iframe src
        setSrc : function( $iframe, index ) {
            
            window.setTimeout( function() {
                
                var $container = $iframe.parent();
                $iframe
                    .remove()
                    .attr( "src", $iframe.attr( otApi.Iframes.dataSrc ) );
                $container.append( $iframe );
                
            }, 1000 * index );
            
        }
        
    };
    
    
    /* Menus
    ------------------------------------------------------------------------- */
    
    otApi.Menus = {
        
        // Settings
        $trigger         : $( ".js-menu" ),
        $currentMenu     : null,
        subMenuClass     : "has-sub-menu",
        openClass        : "is-open",
        openInverseClass : "is-open--inverse",
        currentIndex     : 0,
        subIndex         : 0,
        menuLevel        : 0,
        menuTimer        : null,
        keys             : { tab:     9,
                             enter:  13,
                             escape: 27,
                             space:  32,
                             left:   37,
                             up:     38,
                             right:  39,
                             down:   40
                           },
                           
        // Initialize
        init : function() {
            
            if ( this.$trigger.length ) {
                this.bindUIActions();
            }
            
        },
        
        // Bind UI actions
        bindUIActions : function() {
            
            // Close menus on focus/blur
            this.$trigger.find( "a" )
                .attr("tabindex","0") // Safari fix
                .on( "blur", function() {
                    otApi.Menus.closeMenusOnBlur();
                })
                .on( "focus", function() {
                    otApi.Menus.closeMenusOnFocus( $( this ).parent() );
                });
                
            // Toggle menu on click
            this.$trigger.find( "." + this.subMenuClass + "> a" )
                .on( "click", function( event ) {
                    event.preventDefault();
                    otApi.Menus.toggleMenu( $( this ).parent() );
                });
                
            // Top menu keydown actions
            this.$trigger.find( "> li > a" )
                .on( "focus", function() {
                    var $item = $( this ).parent();
                    otApi.Menus.$currentMenu = $item.parent();
                    otApi.Menus.currentIndex = $item.index();
                })
                .on( "keydown", function( event ) {
                    
                    var $submenu,
                        prevdef = false;
                        
                    switch ( event.keyCode ) {
                        
                        case otApi.Menus.keys.left:
                            
                            otApi.Menus.gotoIndex(
                                otApi.Menus.currentIndex - 1
                            );
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.right:
                            
                            otApi.Menus.gotoIndex(
                                otApi.Menus.currentIndex + 1
                            );
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.enter:
                        case otApi.Menus.keys.space:
                        case otApi.Menus.keys.down:
                            
                            $submenu = $( this ).next();
                            if ( $submenu.length ) {
                                this.click();
                                otApi.Menus.gotoSubIndex( $submenu, 0 );
                                otApi.Menus.menuLevel++;
                                prevdef = true;
                            }
                            break;
                            
                        case otApi.Menus.keys.up:
                            
                            $submenu = $( this ).next();
                            if ( $submenu.length ) {
                                this.click();
                                otApi.Menus.gotoSubIndex(
                                    $submenu, $submenu.children().length - 1
                                );
                                otApi.Menus.menuLevel++;
                                prevdef = true;
                            }
                            break;
                            
                    }
                    
                    if ( prevdef ) {
                        event.preventDefault();
                    }
                    
                });
                
            // Sub-menu keydown actions
            this.$trigger.find( "ul a" )
                .on( "keydown", function( event ) {
                    
                    var $submenu,
                        prevdef = false;
                        
                    switch (event.keyCode) {
                        case otApi.Menus.keys.tab:
                            
                            if ( event.shiftKey ) {
                                otApi.Menus.gotoIndex(
                                    otApi.Menus.currentIndex - 1 );
                            } else {
                                otApi.Menus.gotoIndex(
                                    otApi.Menus.currentIndex + 1 );
                            }
                            
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.left:
                            
                            if ( otApi.Menus.menuLevel > 1 ) {
                                
                                $submenu = $( this ).parent().parent();
                                otApi.Menus.gotoSubIndex(
                                    $submenu.parent().parent(),
                                    $submenu.parent().index()
                                );
                                $submenu.prev().click();
                                
                            } else {
                                
                                otApi.Menus.gotoIndex(
                                    otApi.Menus.currentIndex - 1 );
                                    
                            }
                            
                            otApi.Menus.menuLevel--;
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.right:
                            
                            $submenu = $( this ).next();
                            
                            if ( $submenu.length ) {
                                
                                this.click();
                                otApi.Menus.menuLevel++;
                                otApi.Menus.gotoSubIndex( $submenu, 0 );
                                
                            } else {
                                
                                otApi.Menus.menuLevel = 0;
                                otApi.Menus.gotoIndex(
                                    otApi.Menus.currentIndex + 1 );
                                    
                            }
                            
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.down:
                            
                            $submenu = $( this ).parent().parent();
                            otApi.Menus.gotoSubIndex(
                                $submenu, otApi.Menus.subIndex + 1 );
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.up:
                            
                            $submenu = $( this ).parent().parent();
                            otApi.Menus.gotoSubIndex(
                                $submenu, otApi.Menus.subIndex - 1 );
                            prevdef = true;
                            break;
                            
                        case otApi.Menus.keys.enter:
                        case otApi.Menus.keys.space:
                            
                            $submenu = $( this ).next();
                            
                            if ( $submenu.length ) {
                                
                                this.click();
                                otApi.Menus.menuLevel++;
                                otApi.Menus.gotoSubIndex( $submenu, 0 );
                                prevdef = true;
                                
                            }
                            
                            break;
                            
                        case otApi.Menus.keys.escape:
                            
                            if ( otApi.Menus.menuLevel > 1 ) {
                                
                                $submenu = $( this ).parent().parent();
                                otApi.Menus.gotoSubIndex(
                                    $submenu.parent().parent(),
                                    $submenu.parent().index()
                                );
                                $submenu.prev().click();
                                
                            } else {
                                otApi.Menus
                                    .gotoIndex( otApi.Menus.currentIndex );
                                otApi.Menus.$currentMenu
                                    .children().eq( otApi.Menus.currentIndex )
                                    .children( "a" ).click();
                            }
                            
                            otApi.Menus.menuLevel--;
                            prevdef = true;
                            break;
                            
                    }
                    
                    if ( prevdef ) {
                        event.preventDefault();
                    }
                    
                });
                
            // Reposition menus on window resize
            otApi.RUN_ON_RESIZE.repositionAllMenus = function() {
                otApi.Menus.repositionAllMenus();
            };
            
        },
        
        // Go to top menu item
        gotoIndex : function( index ) {
            
            var len = this.$currentMenu.children().length;
            
            if ( index === len ) {
                index = 0;
            } else if ( index < 0 ) {
                index = len - 1;
            }
            
            this.$currentMenu.children().eq( index ).children( "a" ).focus();
            this.currentIndex = index;
            
        },
        
        // Go to submenu item
        gotoSubIndex : function ( $menu, index ) {
            
            var $items = $menu.find( "> li > a" );
            
            if ( index === $items.length ) {
                index = 0;
            } else if ( index < 0 ) {
                index = $items.length - 1;
            }
            
            $items[index].focus();
            this.subIndex = index;
            
        },
        
        // Toggle menu on/off
        toggleMenu : function( $item ) {
            
            // Close child menus
            if ( $item.hasClass( otApi.Menus.openClass ) ) {
                otApi.Menus.closeMenus(
                    $item.find( "." + otApi.Menus.subMenuClass )
                );
            }
            
            // Reposition menu if beyond window width
            otApi.Menus.repositionMenu( $item );
            
            // Toggle target menu
            $item.toggleClass( otApi.Menus.openClass );
            
            // Toggle aria expanded
            otApi.Menus.toggleAriaExpanded( $item.children( "a" ) );
            
        },
        
        // Close all menus
        closeMenus : function( $nodes ) {
            
            $nodes
                .removeClass( this.openClass + " " + this.openInverseClass )
                .children( "a" )
                .attr( "aria-expanded", function( index, attr ) {
                    if ( attr === "true" ) {
                        return "false";
                    }
                });
                
        },
        
        // Close unrelated menus
        closeUnrelatedMenus : function( $node ) {
            
            if ( $node.length ) {
                
                // Close sibling menus
                this.closeMenus(
                    $node.siblings()
                        .find( "." + this.subMenuClass )
                        .addBack( "." + this.subMenuClass )
                );
                
                // Continue to next parent node
                return this.closeUnrelatedMenus( $node.parent() );
                
            } else {
                return;
            }
            
        },
        
        // Close all menus on blur
        closeMenusOnBlur : function() {
            
            // Add delay for closeMenusOnFocus
            this.menuTimer = window.setTimeout( function() {
                otApi.Menus.closeMenus( $( "." + otApi.Menus.subMenuClass ) );
            }, 10 );
            
        },
        
        // Close unrelated menus on focus
        closeMenusOnFocus : function( $element ) {
            
            // Clear/reset timeout for closeMenusOnBlur
            if ( this.menuTimer ) {
                window.clearTimeout( this.menuTimer );
                this.menuTimer = null;
            }
            
            // Add delay for toggleMenu
            window.setTimeout( function() {
                otApi.Menus.closeUnrelatedMenus( $element );
            }, 10 );
            
        },
        
        // Reposition menu
        repositionMenu : function( $item ) {
            
            var $menu = $item.children( ":last" );
            
            if ( Math.round( $menu.offset().left + $menu.outerWidth() ) >
                $( window ).width()
            ) {
                $item.addClass( otApi.Menus.openInverseClass );
            } else {
                $item.removeClass( otApi.Menus.openInverseClass );
            }
            
        },
        
        // Reposition all menus
        repositionAllMenus : function() {
            
            $( "." + this.openClass )
                // Close all open menus
                .toggleClass( otApi.Menus.openClass )
                .removeClass( otApi.Menus.openInverseClass )
                
                // Reopen menus
                .each( function() {
                    var $this = $( this );
                    $this.addClass( otApi.Menus.openClass );
                    otApi.Menus.repositionMenu( $this );
                });
                
        },
        
        // Toggle aria expanded
        toggleAriaExpanded : function( $element ) {
            
            $element.attr( "aria-expanded", function( index, attr ) {
                if ( attr === "true" ) {
                    return "false";
                } else {
                    return "true";
                }
            });
            
        }
        
    };
    
    
    /* Modal (using Fancybox)
    ------------------------------------------------------------------------- */
    
    otApi.Modal = {
        
        // Settings
        $trigger : $( ".js-modal" ),
        cssFiles : [
            otApi.ROOT_PATH + "/file_source/OpenText/Scripts/jquery.fancybox.min.css"
        ],
        jsFiles  : [
            otApi.ROOT_PATH + "/file_source/OpenText/Scripts/jquery.fancybox.min.js"
        ],
        
        // Initialize
        init : function() {
            if ( this.$trigger.length ) {
                otApi
                    .addCSSFiles( this.cssFiles )
                    .addJSFiles( this.jsFiles, this.resizeVideo );
            }
        },
        
        // Resize modal to 16/9 ratio for iframe videos
        resizeVideo : function() {
            $("[data-fancybox='video'][data-type='iframe'],[data-type='iframe'][data-video]").fancybox({
                beforeLoad : function(instance, current) {
                    current.contentType = "video";
                    current.opts.ratio = 16/9;
                }
            });
        }
        
    };
    
    
    /* Scroll (vertically scroll page content into view)
    ------------------------------------------------------------------------- */
    
    otApi.Scroll = {
        
        // Settings
        $trigger : $(".js-scroll"),
        duration : 2000,
        
        // Initialize
        init : function() {
            if (this.$trigger.length) {
                this.bindUIActions();
            }
        },
        
        // Bind UI actions
        bindUIActions : function() {
            this.$trigger.on("click touchstart", function (event) {
                // Disable default click event
                event.preventDefault();
                
                // Update history state by adding hash to URL
                if (window.location.hash !== this.hash &&
                    typeof history.pushState !== "undefined") {
                    history.pushState(null, null, this.hash);
                }
                
                // Execute scroll
                var duration = $(this).data("duration") || otApi.Scroll.duration;
                otApi.Scroll.scrollPage($(this.hash), duration);
            });
        },
        
        // Scroll page to target element
        scrollPage : function($target, duration) {
            // Use default duration if not set
            if (typeof duration === "undefined" || duration === "") {
                duration = this.duration;
            }
            
            // Execute scroll
            $("html,body").animate({
                scrollTop: $target.offset().top
            }, duration);
        }
        
    };
    
    
    /* Slide (vertically slide content into view)
    ------------------------------------------------------------------------- */
    
    otApi.Slide = {
        
        // Settings
        $activeParent  : null,
        $activeTrigger : null,
        $activeTarget  : null,
        slideSpeed   : "fast",
        slideParent  : ".js-slide-parent",
        slideTrigger : ".js-slide-trigger",
        slideTarget  : ".js-slide-target",
        activeClass  : "active",
        iconClass    : "[class^='icon-'], [class*=' icon-']",
        iconPlus     : "icon-plus",
        iconMinus    : "icon-minus",
        
        // Initialize
        init : function() {
            this.bindUIActions();
        },
        
        // Bind UI actions
        bindUIActions : function() {
            var o = this;
            
            $("body").on("click", this.slideTrigger, function(event) {
                event.preventDefault();
                o.$activeTrigger = $( this );
                o.$activeParent = o.$activeTrigger
                    .closest( o.slideParent );
                o.$activeTarget = o.$activeParent
                    .find( o.slideTarget ).eq(0);
                o.toggleSlide().toggleIcon().toggleAria();
            });
        },
        
        // Toggle active state
        toggleActive : function() {
            this.$activeTrigger
                .add( this.$activeTarget )
                .add( this.$activeParent )
                .toggleClass( this.activeClass );
                
            return this;
        },
        
        // Toggle slide
        toggleSlide : function() {
            if ( this.$activeTarget.hasClass( this.activeClass ) ) {
                // Slide up
                this.$activeTarget.slideUp( this.slideSpeed, function() {
                    // Toggle active class
                    otApi.Slide.toggleActive();
                    otApi.Slide.$activeTarget.css("display", "");
                });
            } else {
                // Toggle active class
                this.toggleActive();
                // Slide down
                this.$activeTarget
                    .hide()
                    .slideDown( this.slideSpeed );
            }
            
            return this;
        },
        
        // Toggle icon
        toggleIcon : function() {
            this.$activeTrigger
                .children( this.iconClass )
                .toggleClass( this.iconPlus + " " + this.iconMinus );
                
            return this;
        },
        
        // Toggle aria expanded
        toggleAria : function() {
            this.$activeTrigger.attr( "aria-expanded", function( index, attr ) {
                if ( attr === "true" ) {
                    return "false";
                } else {
                    return "true";
                }
            });
            
            return this;
        }
        
    };
    
    
    /* Tabs
    ------------------------------------------------------------------------- */
    
    otApi.Tabs = {
        
        // Settings
        $trigger       : $(".js-tabs"),
        tabLinkClass   : "ot-tab-nav__link",
        tabheaderClass : "ot-tab-nav__header",
        tabpanelClass  : "ot-tab-nav__panel",
        activeClass    : "active",
        scrollSpeed    : 400,
        
        // Initialize
        init : function() {
            if (this.$trigger.length) {
                this.bindUIActions();
            }
        },
        
        bindUIActions : function() {
            this.$trigger
                .find("[role='tab']").on("click", function(e) {
                    e.preventDefault();
                    otApi.Tabs.toggleTab($(this));
                });
        },
        
        toggleTab : function($tab) {
            var o = this;
            // var target = $tab.attr("aria-controls"); // WEM removes aria (data attribute used instead)
            var target = $tab.data("controls");
            
            // Don't continue if tab currently active
            if ($tab.hasClass(o.activeClass)) {
                return;
            }
            
            // Toggle active panel and deactivate others
            $("#" + target)
                .toggleClass(o.activeClass)
                .siblings("." + o.tabpanelClass)
                .removeClass(o.activeClass);
                
            // Toggle active tab and deactivate others
            // $("[aria-controls='" + target + "']").each(function() { // WEM removes aria (data attribute used instead)
            $("[data-controls='" + target + "']").each(function() {
                var $this = $(this);
                
                // Toggle aria-selected value
                if ($this.attr("aria-selected") === "false") {
                    $this.attr("aria-selected", "true");
                } else {
                    $this.attr("aria-selected", "false");
                }
                
                if ($this.hasClass(o.tabLinkClass)) {
                    // Toggle tab state (non-mobile)
                    $this
                        .addClass(o.activeClass)
                        .parent()
                        .siblings()
                        .children()
                        .removeClass(o.activeClass)
                        .attr("aria-selected", "false");
                } else {
                    // Toggle accordion state (mobile)
                    $this
                        .toggleClass(o.activeClass)
                        .siblings("." + o.tabheaderClass)
                        .removeClass(o.activeClass)
                        .attr("aria-selected", "false");
                        
                    // Scroll up to show tab at top of window
                    if (otApi.isMobile()) {
                        $("html, body").animate({
                            scrollTop: $this.offset().top
                        }, o.scrollSpeed);
                    }
                }
            });
        }
        
    };
    
    
    /* Vidyard player embed
    ------------------------------------------------------------------------- */
    
    otApi.Vidyard = {
        
        // Settings
        $trigger : $( ".js-embed-vidyard" ),
        jsFiles  : [
            "https://play.vidyard.com/embed/v4.js"
        ],
        
        // Initialize
        init : function() {
            
            if ( this.$trigger.length && !window.vidyardEmbed ) {
                
                // Load Vidyard JS file
                otApi.addJSFiles(this.jsFiles);
                
            }
            
        }
        
    };
    
    
    /* Window Resize (run actions on window resize)
    ------------------------------------------------------------------------- */
    
    otApi.WindowResize = {
        
        // Settings
        resizing : null,
        
        // Initialize
        init : function() {
            this.bindUIActions();
        },
        
        // Bind UI actions
        bindUIActions : function() {
            $( window ).on( "resize", this.resize );
        },
        
        // Throttle resizing to delay running actions
        resize : function() {
            // Clear timeout
            if ( this.resizing ) {
                window.clearTimeout( this.resizing );
                this.resizing = null;
            }
            
            // Set timeout
            this.resizing = window.setTimeout( otApi.WindowResize.done, 100 );
        },
        
        // Run actions on resize completion
        done : function() {
            var key;
            
            // Clear timeout
            window.clearTimeout( this.resizing );
            this.resizing = null;
            
            // Get breakpoint if needed for actions
            otApi.ACTIVE_BREAKPOINT = otApi.BreakpointWatcher.getBreakpoint();
            
            // Run actions
            for ( key in otApi.RUN_ON_RESIZE ) {
                otApi.RUN_ON_RESIZE[ key ]();
            }
        }
        
    };
    
    
    /* Initialize
    ------------------------------------------------------------------------- */
    
    // Load iframes
    otApi.Iframes.init();
    
    // DOM ready
    $( function() {
        
        // Assign breakpoint to global var
        otApi.ACTIVE_BREAKPOINT = otApi.BreakpointWatcher.getBreakpoint();
        
        // Assign url params to global var
        otApi.URL_PARAMS = otApi.getUrlParams();
        
        // Run initialization scripts
        otApi.Carousel.init();
        otApi.Clickable.init();
        otApi.GridOverlay.init();
        otApi.Hamburger.init();
        otApi.Menus.init();
        otApi.Modal.init();
        otApi.Scroll.init();
        otApi.Slide.init();
        otApi.Tabs.init();
        otApi.Vidyard.init();
        otApi.WindowResize.init();
        
    });
    
}( window.jQuery, window.otApi = window.otApi || {} ) );
