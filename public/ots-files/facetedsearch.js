;(function($){
    $.FacetingItem = function(){
        this.defaults = {
            items              : [{a:2,b:1,c:2},{a:2,b:2,c:1},{a:1,b:1,c:1},{a:3,b:3,c:1}],
            facets             : {'a': 'Title A', 'b': 'Title B', 'c': 'Title C'},
            resultSelector     : '#results',
            facetSelector      : '#facets',
            orderBySelector    : null,
            usedListSelector   : null,
            facetContainer     : '<div class=facetsearch id=<%= id %> ></div>',
            facetTitleTemplate : '<h3><%= title %></h3>',
            facetListContainer : '<div class=facetlist></div>',
            listItemTemplate   : '<div class=facetitem id="<%= id %>"><%= name %> <span class=facetitemcount>(<%= count %>)</span></div>',
			SFacetListContainer: '<select class="form__field facets"><option value="">All</option></select>',
			SListItemTemplate  : '<option class="form__choice facetitem" id="<\%= id %>" value="<\%= name %>"><\%= name %></option>',
            usedListTemplate   : '<a href="javascript:void(0);" data-filtername="<%= attrFiltername %>" data-facet="<%= attrFacet %>" class=uselistitem><%= name %></a>',
            bottomContainer    : '<div class=bottomline></div>',
            orderByTemplate    : '<div class=orderby><span class="orderby-title">Sort by: </span><ul><% _.each(options, function(value, key) { %>'+
                                '<li class=orderbyitem id=orderby_<%= key %>>'+
                                '<%= value %> </li> <% }); %></ul></div>',
            countTemplate      : '<div class=facettotalcount><%= count %> Results</div>',
            deselectTemplate   : '<div class=deselectstartover>Deselect all filters</div>',
            resultTemplate     : '<div class=facetresultbox><%= name %></div>',
            noResults          : '<li class=no_results>Sorry, but no items match these criteria</li>',
            orderByOptions     : {'a': 'by A', 'b': 'by B', 'RANDOM': 'by random'},
            state              : {
                                    orderBy : false,
                                    filters : {}
                                },
            showMoreTemplate   : '<a id=showmorebutton>Show more</a>',
            enablePagination   : true,
            paginationCount    : 20
        };
        this.settings = {};
        this.moreButton;
    };
    $.FacetingItem.prototype = {
        InitEvents: function () {
    
        },
        /**
        * This is the first function / variable that gets exported into the 
        * jQuery namespace. Pass in your own settings (see above) to initialize
        * the faceted search
        */
    
        facetelize : function(usersettings) {
            $.extend(this.settings, this.defaults, usersettings);
            this.settings.currentResults = [];
            this.settings.facetStore     = {};
            $(this.settings.facetSelector).data("settings", this.settings);
            this.initFacetCount();
            this.filter();
            this.order();
            this.createFacetUI();
            this.updateResults();
        },
        /**
        * This is the second function / variable that gets exported into the 
        * jQuery namespace. Use it to update everything if you messed with
        * the settings object
        */
        facetUpdate : function() {
            this.filter();
            this.order();
            this.updateFacetUI();
            this.updateResults();
        },
        /**
        * The following section contains the logic of the faceted search
        */

        /**
        * initializes all facets and their individual filters 
        */
        initFacetCount : function () {
            var me = this;
            _.each(me.settings.facets, function(facettitle, facet) {
                me.settings.facetStore[facet] = {};
            });
            _.each(me.settings.items, function(item) {
                // intialize the count to be zero
                _.each(me.settings.facets, function(facettitle, facet) {
                    if ($.isArray(item[facet])) {
                    _.each(item[facet], function(facetitem) {
                        me.settings.facetStore[facet][facetitem] = me.settings.facetStore[facet][facetitem] || {count: 0, id: _.uniqueId("facet_")}
                    });
                    } else {
                    if (item[facet] !== undefined) {
                        me.settings.facetStore[facet][item[facet]] = me.settings.facetStore[facet][item[facet]] || {count: 0, id: _.uniqueId("facet_")}
                    }
                    }
                });
            });
            // sort it:
            _.each(me.settings.facetStore, function(facet, facettitle) {
            var sorted = _.keys(me.settings.facetStore[facettitle]);
            
            if (typeof(_.keys(me.settings.facetStore[facettitle])[0]) === 'string') {
            	sorted = sorted.sort(function (a, b) {
            		return a.toLowerCase().localeCompare(b.toLowerCase());
            	});
            } else {
            	sorted.sort();
            }
            
            if (me.settings.facetSortOption && me.settings.facetSortOption[facettitle]) {
                sorted = _.union(me.settings.facetSortOption[facettitle], sorted);
            }
            var sortedstore = {};
            _.each(sorted, function(el) {
                sortedstore[el] = me.settings.facetStore[facettitle][el];
            });
            me.settings.facetStore[facettitle] = sortedstore;
            });
        },
        /**
        * resets the facet count
        */
        resetFacetCount : function() {
            var me = this;
            _.each(me.settings.facetStore, function(items, facetname) {
                _.each(items, function(value, itemname) {
                    me.settings.facetStore[facetname][itemname].count = 0;
                });
            });
        },
        /**
        * Filters all items from the settings according to the currently 
        * set filters and stores the results in the settings.currentResults.
        * The number of items in each filter from each facet is also updated
        */
        filter : function() {
            // first apply the filters to the items
            var me = this;
            me.settings.currentResults = _.select(me.settings.items, function(item) {
            var filtersApply = true;
            _.each(me.settings.state.filters, function(filter, facet) {
                if ($.isArray(item[facet])) {
                    var inters = _.intersect(item[facet], filter);
                    if (inters.length == 0) {
                        filtersApply = false;
                    }
                } else {
                    if (filter.length && _.indexOf(filter, item[facet]) == -1) {
                        filtersApply = false;
                    }
                }
            });
            return filtersApply;
            });
            // Update the count for each facet and item:
            // intialize the count to be zero
            this.resetFacetCount();
            // then reduce the items to get the current count for each facet
            _.each(me.settings.facets, function(facettitle, facet) {
                _.each(me.settings.currentResults, function(item) {
                    if ($.isArray(item[facet])) {
                        _.each(item[facet], function(facetitem) {
                            me.settings.facetStore[facet][facetitem].count += 1;
                        });
                    } else {
                        if (item[facet] !== undefined) {
                            me.settings.facetStore[facet][item[facet]].count += 1;
                        }
                    }
                });
            });
            // remove confusing 0 from facets where a filter has been set
            _.each(me.settings.state.filters, function(filters, facettitle) {
                _.each(me.settings.facetStore[facettitle], function(facet) {
                    if (facet.count == 0 && me.settings.state.filters[facettitle].length) facet.count = "+";
                });
            });
            me.settings.state.shownResults = 0;
            this.updateUsedList();
        },
        /**
        * Orders the currentResults according to the settings.state.orderBy variable
        */ 
        order : function() {
            var me = this;
            if (me.settings.state.orderBy) {
                $(me.settings.orderBySelector + " .activeorderby").removeClass("activeorderby");
                $(me.settings.orderBySelector + ' #orderby_'+me.settings.state.orderBy).addClass("activeorderby");
                //check if a date sort
                if(me.settings.state.orderBy.toLowerCase().indexOf("date") >= 0){
                    me.settings.currentResults = _.sortByDate(me.settings.currentResults, function(item) {
                    	if(me.settings.resultSelector.toLowerCase().indexOf(".upcoming") >= 0){
							 return -item[me.settings.state.orderBy];
						}
                        return item[me.settings.state.orderBy];
                    });
                }else{
                    //normal sort
                    me.settings.currentResults = this.sortBy(me.settings.currentResults, function(item) {
                        if (me.settings.state.orderBy == 'RANDOM') {
                            return Math.random()*10000;
                        } else if (typeof(item[me.settings.state.orderBy]) === "string") {
								return item[me.settings.state.orderBy].toLowerCase();
                        } else {
                        	return item[me.settings.state.orderBy];
                        }
                    });
                }
            }
        },
        /**
        * The given facetname and filtername are activated or deactivated
        * depending on what they were beforehand. This causes the items to
        * be filtered again and the UI is updated accordingly.
        */
        toggleFilter : function (key, value) {
            var me = this;
            me.settings.state.filters[key] = me.settings.state.filters[key] || [] ;
            if (_.indexOf(me.settings.state.filters[key], value) == -1) {
                me.settings.state.filters[key].push(value);
            } else {
                me.settings.state.filters[key] = _.without(me.settings.state.filters[key], value);
                if (me.settings.state.filters[key].length == 0) {
                    delete me.settings.state.filters[key];
                }
            }
            this.filter();
        },
        /**
        * The following section contains the presentation of the faceted search
        */

        /**
        * This function is only called once, it creates the facets ui.
        */
        createFacetUI : function () {
            var me = this;
            var itemtemplate  = _.template(me.settings.listItemTemplate);
            var titletemplate = _.template(me.settings.facetTitleTemplate);
            var containertemplate = _.template(me.settings.facetContainer);
			var sfacettemplate    = _.template(me.settings.SListItemTemplate);
  
            $(me.settings.facetSelector).html("");
            _.each(me.settings.facets, function(facettitle, facet) {
                var facetHtml     = $(containertemplate({id: facet}));
                var facetItem     = {title: facettitle};
                var facetItemHtml = $(titletemplate(facetItem));

                facetHtml.append(facetItemHtml);
                var facetlist = $(me.settings.facetListContainer);
				if(me.settings.agendaBuilder){
					facetlist = $(me.settings.SFacetListContainer);
					itemtemplate = sfacettemplate;
				  }
                _.each(me.settings.facetStore[facet], function(filter, filtername){
                    if(filtername != null && filtername != "" && typeof filtername !== "undefined"){
                        var item = {id: filter.id, name: filtername, count: filter.count};
                        var filteritem  = $(itemtemplate(item));
                        if (_.indexOf(me.settings.state.filters[facet], filtername) >= 0) {
                            filteritem.find(':checkbox').attr('checked','checked');
                            filteritem.addClass("activefacet");
                        }
                        facetlist.append(filteritem);
                    }
                });
                facetHtml.append(facetlist);
                $(me.settings.facetSelector).append(facetHtml);
            });
            // add the click event handler to each facet item:
            //$('.facetitem').click(function(event){
			$(document).on("click", ".facetitem", function(event){
                event.preventDefault();
                var filter = me.getFilterById(this.id);
                me.toggleFilter(filter.facetname, filter.filtername);
                $(me.settings.facetSelector).trigger("facetedsearchfacetclick", filter);
                me.order();
                me.updateFacetUI();
                me.updateResults();
            });
            // Append total result count
            var bottom = $(me.settings.bottomContainer);
            //countHtml = _.template(me.settings.countTemplate, {count: me.settings.currentResults.length});
            //$(bottom).append(countHtml);
            // generate the "order by" options:
            var ordertemplate = _.template(me.settings.orderByTemplate);
            var itemHtml = $(ordertemplate({'options': me.settings.orderByOptions}));
            if(me.settings.orderBySelector != null){
                $(me.settings.orderBySelector).html(itemHtml);
            }else{
                $(bottom).append(itemHtml);
            }
            $(me.settings.facetSelector).append(bottom);
            $('.orderbyitem').each(function(){
                var id = this.id.substr(8);
                if (me.settings.state.orderBy == id) {
                    $(this).addClass("activeorderby");
                }
            });
            // add the click event handler to each "order by" item:
            if($(me.settings.orderBySelector + ' .orderbyitem').parent().prop("tagName").toLowerCase() == 'select'){
                $(me.settings.orderBySelector + ' select').change(function(event){
                    var id = $(this).val();
                    me.settings.state.orderBy = id;
                    $(me.settings.facetSelector).trigger("facetedsearchorderby", id);
                    me.settings.state.shownResults = 0;
                    me.order();
                    me.updateResults();
                });
            }else{
                $(me.settings.orderBySelector + ' .orderbyitem').click(function(event){
                    var id = this.id.substr(8);
                    me.settings.state.orderBy = id;
                    $(me.settings.facetSelector).trigger("facetedsearchorderby", id);
                    me.settings.state.shownResults = 0;
                    me.order();
                    me.updateResults();
                });
            }
  
            // Append deselect filters button
            var deselect = $(me.settings.deselectTemplate).click(function(event){
            me.settings.state.filters = {};
            jQuery.facetUpdate();
            });
            //$(bottom).append(deselect);
            $(me.settings.facetSelector).trigger("facetuicreated");
        },
        /**
        * get a facetname and filtername by the unique id that is created in the beginning
        */
        getFilterById : function(id) {
            var me = this;
            var result = false;
            _.each(me.settings.facetStore, function(facet, facetname) {
                _.each(facet, function(filter, filtername){
                    if (filter.id == id) {
                    result =  {'facetname': facetname, 'filtername': filtername};
                    }
                });
            });
            return result;
        },
        /**
        * This function is only called whenever a filter has been added or removed
        * It adds a class to the active filters and shows the correct number for each
        */
        updateFacetUI :function () {
            var me = this;
			var sfacettemplate    = _.template(me.settings.SListItemTemplate);
			if(me.settings.agendaBuilder){
				var itemtemplate = sfacettemplate;
			}else{
				var itemtemplate = _.template(me.settings.listItemTemplate);	
			}
            
            _.each(me.settings.facetStore, function(facet, facetname) {
            _.each(facet, function(filter, filtername){
                var item = {id: filter.id, name: filtername, count: filter.count};
                var filteritem  = $(itemtemplate(item)).html();
                $("#"+filter.id).html(filteritem);
                if (me.settings.state.filters[facetname] && _.indexOf(me.settings.state.filters[facetname], filtername) >= 0) {
                $("#"+filter.id).find(':checkbox').attr('checked','checked');
                $("#"+filter.id).addClass("activefacet");
                } else {
                $("#"+filter.id).removeClass("activefacet");
                }
            });
            });
            countHtml = _.template(me.settings.countTemplate, {count: me.settings.currentResults.length});
            $(me.settings.facetSelector + ' .facettotalcount').replaceWith(countHtml);
        },
        /**
        * Updates the the list of results according to the filters that have been set
        */
        updateResults : function () {
            var me = this;
            $(me.settings.resultSelector).html(me.settings.currentResults.length == 0 ? me.settings.noResults : "");
            this.showMoreResults(me);
        },
        /**
        * This function is only called whenever a filter has been added or removed
        * It adds a list of the used filters to specified location
        */
        updateUsedList : function () {
            var me = this;
            if(me.settings.usedListSelector != null){
                $(me.settings.usedListSelector).html("");
                var usedItemtemplate = _.template(me.settings.usedListTemplate);
                var index = 0;
                _.each(me.settings.state.filters, function(filter, facet) {
                _.each(filter, function(filtername){
                    var item = {attrFiltername: filtername, attrFacet: facet, name: filtername};
                    var usedFilteritem  = $(usedItemtemplate(item));
                    $(me.settings.usedListSelector).append(usedFilteritem);
                    $(me.settings.usedListSelector).append(",");
                    index++;
                });    
                });
                //remove last comma
                $(me.settings.usedListSelector).html($(me.settings.usedListSelector).html().substring(0, $(me.settings.usedListSelector).html().length - 1));
            }

            //add the click event handler removal of used list item:
            $('.uselistitem').click(function(event){
                event.preventDefault();
                var dataFacet = $(this).attr('data-facet');
                var dataFiltername = $(this).attr('data-filtername');
                //call toggleFilter this will remove it from state list
                toggleFilter(dataFacet, dataFiltername);
                this.order();
                this.updateFacetUI();
                this.updateResults();
            });
        },
        showMoreResults : function (instance) {
            var me = instance;
            var showNowCount = 
                me.settings.enablePagination ? 
                Math.min(me.settings.currentResults.length - me.settings.state.shownResults, me.settings.paginationCount) : 
                me.settings.currentResults.length;
            var itemHtml = "";
            var template = _.template(me.settings.resultTemplate);
            for (var i = me.settings.state.shownResults; i < me.settings.state.shownResults + showNowCount; i++) {
            var item = $.extend(me.settings.currentResults[i], {
                totalItemNr    : i,
                batchItemNr    : i - me.settings.state.shownResults,
                batchItemCount : showNowCount
            });
            var itemHtml = itemHtml + template(item);
            }
            $(me.settings.resultSelector).append(itemHtml);
            if (!me.moreButton) {
                me.moreButton = $(me.settings.showMoreTemplate).click(function(event){me.showMoreResults(me);});
            $(me.settings.resultSelector).after(me.moreButton);
            }
            if (me.settings.state.shownResults == 0) {
                me.moreButton.show();
            }
            me.settings.state.shownResults += showNowCount;
            if (me.settings.state.shownResults == me.settings.currentResults.length) {
                $(me.moreButton).hide();
            }
            $(me.settings.resultSelector).trigger("facetedsearchresultupdate");
        },
        
        //Fix for ITAPPS-2620 show null/empty values last
        sortBy:function(obj, iterator, context) {
        	return _.pluck(_.map(obj, function (value, index, list) {
        		return {
        			value: value,
        			criteria: iterator.call(context, value, index, list)
        		};
        	}).sort(function (left, right) {
        		var a = left.criteria, b = right.criteria;
        		if(a === null || a === "" || a.length === 0){
        			return 1;
        		}else if(b === null || b === "" || b.length === 0){
        			return -1;
        		}
        		if(a === b) return 0;
        		return a < b ? -1 : 1;
        	}), 'value');
        }

    };
/**
    * Please note that when passing in custom templates for 
    * listItemTemplate and orderByTemplate to keep the classes as
    * they are used in the code at other locations as well.
    */

	if($('body').find('.search-facets-new').length > 0 || $('body').find('.faceted-search__filters').length > 0){  
	//if(settings.agendaBuilder){ 
	window.delayedJavascriptArray = window.delayedJavascriptArray || [];
	window.delayedJavascriptArray.push(function() {

        
        // Date HTML
        function createDateFacetHTML($this) {
            var active  = $this.hasClass("activefacet"),
                checked = "",
                html    = "";
                
            if (active) {
                checked = " checked";
            }
            
            html =
                '<li class="' + $this.attr("class") + '" id="' + $this.attr("id") + '">' +
                '<label for="' + $this.attr("id") + '">' +
                '<div class="table-cell"><input type="checkbox"' + checked + '></div>' +
                '<div class="table-cell" data-day="'+$this.text().replace(/( 0)(\d)/, ' $2').split(",")[0]+'"><a href="javascript:void(0);" >' +
                $this.text().replace(/( 0)(\d)/, ' $2').split(",")[0] +
                '</a></div></label></li>';
                
            return html;
        };
        
        // Select boxes HTML
        function adjustSelectBoxes() {
            var item,
                targets = {
                    "track"       : "Tracks",
                    "industry"    : "Industries",
                    "sessionType" : "Session Types",
                    "audience"    : "Audiences"
                },
				pstargets = {
                    "partnerName"       : "Partners",
                    "solutionName"    	: "Solutions",
                    "industries" 		: "Industries",
                    "products"    		: "Products"
                },
				facets = settings.facets;
			
			if(settings.isPartnerSolutions){
				facets = pstargets;
			}
			
			if(settings.agendaBuilder){
				facets = targets;
			}
				
			for (item in facets) {
                $("#" + item)
                    .find("label").attr("for", item + "-facet")
                    .next().attr("id", item + "-facet")
                    .find("option:first").text("All " + facets[item])
                    .parent()
                    .find(".activefacet").attr("selected", "")
                    .parent()
                    .find("option").not(".activefacet").removeAttr("selected");
            }
        };
        
        // Initialize facet UI modifications
        $(settings.facetSelector).bind("facetuicreated", function() {
            var $date = $("#date"),
                label = $date.find(".form__label");
                $list = $('<ul class="facets list--inline mobile-res" />').appendTo($date);
                
            $date.find("option").slice(1).each( function() {
                $list.append( createDateFacetHTML($(this)) );
            });
            
            $date
                .addClass("one-whole")
                .removeClass("md-one-half")
                .find("select")
                .remove()
                .end()
                .wrap("<fieldset />");
                
                label.replaceWith('<legend class="' + label.attr("class") + '">' + label.text() + '</legend>')
                
            $(document).find("select .facetitem").removeClass("facetitem");
            
            adjustSelectBoxes();
        });
        
        // Rebuild facet UI when results updated
        $(settings.resultSelector).bind("facetedsearchresultupdate", function() {
            
            // For testing
            // window.console.log(settings.state.filters);
            
            $("#date li").each( function() {
                $(this).replaceWith( createDateFacetHTML($(this)) );
            });
            
            adjustSelectBoxes();
        });
        
        // Set onchange event to select boxes
        $(document).on("change", "select.facets", function(event) {
            var $target = $(event.target),
                id      = $target.parent().attr("id"),
                value   = $target.children(":selected").val(),
                filter  = facetedsearchObject.getFilterById(id);
                
            if (value) {
                settings.state.filters[id] = [value];
            } else {
                delete settings.state.filters[id];
            }
            
            $(settings.facetSelector).trigger("facetedsearchfacetclick", filter);
            facetedsearchObject.facetUpdate();
        });
        
    });
}




    

}(jQuery));
