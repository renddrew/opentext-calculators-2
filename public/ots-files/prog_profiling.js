//Contains the functions used for progressive profiling. Requires jQuery.

/*
 * Mark the link as having been accessed (servlet sets some cookies to do this)
 */
function markAccessed(linkid) {
	$.ajax({
		type: "POST",
		url: "/ot-itapps-www-dpm/asset-gate?action=markAccessed&linkid="+linkid,
		async: false
	});
}
function parseXml(xml,linkid,hasLandingPage,furl) {
	//If we were allowed to access the asset, forward to it.
	//Otherwise, pop up a colorbox to the URL for the form or
	//forward to the landing page (indicated by furl) for the asset if it has one.
$(xml).find("GateResult").each(function() {
		if ($(this).attr("allow") === "true") {
			markAccessed(linkid);
			//Redirect to asset
			window.location.href=$(this).attr("link");
		} else {
			if (hasLandingPage) {
				window.location.href=furl;
			} else {
				var formWidth = $(this).attr("width");
				var formHeight = $(this).attr("height");
				var formHTML = "<iframe marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" ";
				if (formWidth != null) {
					formHTML = formHTML + "width=\"" + formWidth + "\" ";
				}
				if (formHeight != null) {
					formHTML = formHTML + "height=\"" + formHeight + "\" ";
				}
				formHTML = formHTML + "src=\"" + $(this).attr("formURL") + "\" ></iframe>"
				//if the window size is less than 720 we are in mobile
				//and want to just go to the page
				if ($(window).width() >= 720) {
					var width = formWidth;
					var height = formHeight;

					// grab the modal code and move it next to the overlay.
					$('.final-modal-contents').html('<a class="close" href="#" style="display: block;">Close</a>' + formHTML);

					$('.modal-overlay').show();
					$('.final-modal-contents').contents().hide();
					$('.final-modal-contents').css('height', '100px').css('width', '100px').css('right', '28%').show().animate({ height: height }, 400).animate({ width: width }, 400, function () {
						$('.final-modal-contents').contents().fadeIn();
						// we anchor the close functionality only once the contents are already there.
						$('.close').click(function (event) {
							event.preventDefault();
							$('.final-modal-contents').hide();
							$('.modal-overlay').hide();
						});
					});
					$('.modal-overlay').click(function () {
						$('.final-modal-contents').hide();
						$('.modal-overlay').hide();
					});
					//Show the colorbox, which will have an iframe in it
					// $.colorbox({
						// html: formHTML, innerWidth: formWidth, innerHeight: formHeight,
						// initialWidth: formWidth, initialHeight: formHeight,
						// transition: 'fade',
						//Hide the initial loading popup
						// onOpen: function() {
							// $('#colorbox').css('opacity',0.0);
						// }
					// });
				}else{
					window.location.href=$(this).attr("formURL");				
				}
            }
		}
	});
}
/*
 * Does the query to see if user has access to the asset pointed at
 * by the link with id "linkid". If the user has access, it will
 * simply redirect them to the asset. If they don't and hasLandingPage is true,
 * the user will be redirected to the url specified by the "furl" parameter. Otherwise,
 * a lightbox will pop up with the form the user must fill out to gain access.
 * 
 */
function doQueryAndDisplay(linkid,hasLandingPage,furl) {
	var cc_cookie = getCookie('cc_advertising');
    var cc_ineu = getCookie('cc_ineu');
    if (cc_cookie == 'yes' || cc_ineu == 'no' || cc.approved['cc_advertising'] == 'yes') {
		$.ajax({
			type: "POST",
			url: "/ot-itapps-www-dpm/asset-gate?action=gateCheck&linkid="+linkid,
			dataType: "xml",
			success: function(xml) {
				parseXml(xml,linkid,hasLandingPage,furl);
			}
		});
    } else {
    	var formHTML = '<div class="notice euCookieMessage">This page element sets <!-- cc- -->advertising<!-- -cc --> cookies. <a class="cookieSettings" href="#">Change your cookie settings</a> to allow cookies and show this content.</div>';
    	// grab the modal code and move it next to the overlay.
		$('.final-modal-contents').html('<a class="close" href="#" style="display: block;">Close</a>' + formHTML);

        replaceCookieCategoryNames()

		$('.modal-overlay').show();
		//$('.final-modal-contents').contents().hide();
		
		var width = $('.final-modal-contents').css('width');
        var height = $('.final-modal-contents').css('height');
		$('.final-modal-contents').css('height', '200px').css('width', '200px').css('right', '28%').show().animate({ height: height }, 400).animate({ width: width }, 400, function () {
			$('.final-modal-contents').contents().fadeIn();
			// we anchor the close functionality only once the contents are already there.
			$('.close').click(function (event) {
				event.preventDefault();
				$('.final-modal-contents').hide();
				$('.modal-overlay').hide();
			});
		});
		$('.modal-overlay').click(function () {
			$('.final-modal-contents').hide();
			$('.modal-overlay').hide();
		});
    }
}

//Helpful method for getting variables from a url. returns
//null if not found. Decodes any URL encoded vars and also decodes
//any html entities
function getQueryVariable(url,variable) {
  var query = url;
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      var decoded = $('<div/>').html(decodeURIComponent(pair[1])).text();
      return decodeURIComponent(decoded);
    }
  } 
  return null;
}

$(document).ready(function() {
	//If the doclick parameter is specified, this means
	//the user probably middle-mouse clicked the link.
	//In this case, do the click immediately
	if (window.location.search.indexOf("doclick") != -1) {
		doQueryAndDisplay(getQueryVariable(window.location.search,"linkid"),
				getQueryVariable(window.location.search,"landingpage") != null,
				getQueryVariable(window.location.search,"furl"));
	}
	
	//When the anchor tag is clicked,
	//we will do an ajax query to see
	//if the asset is gated
	$(".gated-link").click(function(e) {
		e.preventDefault();
		//Pass the id of the link to the doQuery function, along with the
		//channel id, 
		doQueryAndDisplay(getQueryVariable($(this).attr("href"),"linkid"),
				getQueryVariable($(this).attr("href"),"landingpage") != null,
				getQueryVariable($(this).attr("href"),"furl"));
	});
});

