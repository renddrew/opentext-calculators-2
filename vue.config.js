const prefixer = require('postcss-prefix-selector')
module.exports = {

  // dont do linting
  lintOnSave: false,


  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    output: {
      filename: 'ar-ot-calc.js'
      //path: path.resolve(__dirname, 'dist')
    },
  },
  css: {
    extract: false,
    loaderOptions: {
      postcss: {
        plugins: [
          prefixer({
             prefix: '.ar-ot-calc',
          })
        ]
      }
    }
  },
}